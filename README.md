# Dracula for [Openbox](http://openbox.org/wiki/Main_Page)

> A dark theme for Openbox WM based on the [Dracula color scheme](https://draculatheme.com). It is compatible with the Ant-Dracula GTK and KDE themes.

**Theme with border**
![Theme with border](dracula_withoutborder.png)

<hr>

**Borderless theme**
![Borderless theme](dracula_withborder.png)

<hr>

**Menu theme**<br>
![Menu](context-menu.png)


## Install
Place the repository folders: **Dracula** and **Dracula-withoutBorder** in the <code>/home/your-user/.themes</code> directory and select with the **Openbox configuration** the theme of your preference.

To install the menu, first install with Muon/Synaptic package manager: <code>openbox-menu</code> and <code>lxmenu-data</code> (o en su defecto hacer un <code>sudo apt-get install openbox-menu lxmenu-data</code>) and copy the file **menu.xml** to the <code>/home/your-user/.config/openbox</code> folder depending on your desktop language and whether it is LXDE or LXQt (this so that it shows the corresponding applications and options that each menu has in its DE). 

## Team

This theme is maintained by the following person(s) and a bunch of [awesome contributors](https://github.com/dracula/template/graphs/contributors).

| [![César Salazar](https://gitlab.com/uploads/-/system/user/avatar/4406807/avatar.png?)](https://gitlab.com/the-zero885) |
|---|
| <div align="center">**César Salazar**<br>[Gitlab Profile](https://gitlab.com/the-zero885)<br>[Github Profile](https://github.com/the-zero885)</div> |

## License

[MIT License](./LICENSE)
